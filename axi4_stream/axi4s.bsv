package axi4s ;

  import axi4s_fabric  :: * ;
  import axi4s_types   :: * ;

  export axi4s_fabric  :: * ;
  export axi4s_types   :: * ;
endpackage: axi4s
